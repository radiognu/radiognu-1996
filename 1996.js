/* BufferLoader: Esta clase se encarga de obtener de forma asíncrona una lista
  de recursos, para luego ser cargados dentro de un contexto de WebAudio.
  Argumentos:
    context: El contexto de WebAudio a utilizar.
    urlList: Un array con una lista de recursos a cargar.
    callback: La función a ejecutar una vez se hayan cargado todos los
           recursos.
  Atributos:
    context: El contexto de WebAudio a utilizar.
    urlList: Un array con una lista de recursos a cargar.
    onload: Almacena la función a ejecutar cuando se dispare el evento "load".
    bufferList: Un array con una lista de recursos cargados.
    loadCount: Un indicador de cuantos elementos se han obtenido. */
function BufferLoader(context, urlList, callback) {
    this.context = context;
    this.urlList = urlList;
    this.onload = callback;
    this.bufferList = new Array();
    this.loadCount = 0;
}

/* BufferLoader.loadBuffer(): Esta función se encarga de crear una solicitud
  asíncrona para comenzar a descargar el elemento indicado en el argumento
  «url», y lo almacena en la posición «index» del buffer. */
BufferLoader.prototype.loadBuffer = function (url, index) {
    /* request es el objeto que adminstrará la descarga. */
    var request = new XMLHttpRequest();
    /* Se inicializa la solicitud del recurso. El tercer argumento indica que
      tal solicitud es asíncrona. */
    request.open("GET", url, true);
    /* Se indica que esperamos datos del tipo «arraybuffer». */
    request.responseType = "arraybuffer";

    var loader = this;

    /* Lo siguiente se ejecuta cuando el evento «load» se dispare, es decir,
      cuando el recurso haya terminado de ser descargado. */
    request.onload = function () {
        /* AudioContext.decodeAudioData() se encarga de decodificar el
          recurso descargado en un formato que sea legible para el contexto
          de Web Audio API. El primer argumento son los datos de audio del
          recurso. El segundo argumento es la función que se ejecuta una vez
          cargado el recurso en memoria. El tercer argumento es la función
          que se ejecuta cuando ocurra un error antes o durante la
          decodificación. Usamos la sintaxis antigua para definir el
          callback por retrocompatibilidad. */
        loader.context.decodeAudioData(
            request.response,
            function (buffer) {
                /* Si el recurso no es válido, se muestra un mensaje de
                  error y se termina la ejecución de la función. */
                if (!buffer) {
                    alert('error decoding file data: ' + url);
                    return;
                }
                /* Si todo ha salido bien, entonces almacenamos el buffer
                  dentro de la lista «bufferList», en la posición
                  indicada en «index». */
                loader.bufferList[index] = buffer;
                /* Si ya hemos cargado todos los recursos de la lista
                  «urlList», entonces disparamos manualmente el evento
                  «load». Ojo con el ++, que suma «loader.loadCount»
                  ANTES de hacer la comparación. */
                if (++loader.loadCount == loader.urlList.length)
                    loader.onload(loader.bufferList);
            },
            function (error) {
                console.error('decodeAudioData error', error);
            }
        );
    }

    /* Lo siguiente se ejecuta cuando el evento «error» se dispare, es decir,
      cuando el recurso haya fallado antes o durante su descarga. */
    request.onerror = function () {
        alert('BufferLoader: XHR error');
    }

    /* Se inicia la descarga del recurso. */
    request.send();
}

/* BufferLoader.load(): Esta función simplemente itera sobre la lista «urlList»
  para descargar el recurso correspondiente. No confundir con el evento
  «load» (onload) */
BufferLoader.prototype.load = function () {
    for (var i = 0; i < this.urlList.length; ++i)
        this.loadBuffer(this.urlList[i], i);
}

/* ready(): Esta función se encarga de emular el $(document).ready() */
function ready(fn) {
    if (document.readyState != 'loading') {
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

/* Lo siguiente se ejecuta al principio. */
ready(function () {
    /* Obtenemos el elemento que contiene el audio de "conexión vía modem" */
    var modem = document.getElementById("modem");
    /* Seleccionamos el contexto de WebAudio correspondiente al navegador.
      Si, incluso "ese" navegador. */
    var contextClass = (window.AudioContext || window.webkitAudioContext ||
        window.mozAudioContext || window.oAudioContext || window.msAudioContext
    );

    /* Si «contextClass» es válido, quiere decir que el navegador soporta
      WebAudio, por lo que creamos un contexto y procedemos a inicializarlo */
    if (contextClass) {
        context = new contextClass();
        initWebAudio(context, modem, document.getElementById("radio"));
    }
    /* En caso contrario, inicializamos un "fallback", que consiste en cargar
      la versión AM del flujo de la radio. */
    else {
        initHTML5Audio(modem, document.getElementById("radio-fallback"));
    }

});

/* Definimos «radioSource» como global, para que «readyToRun()» pueda trabajar
  con la fuente del flujo de audio.
  Definimos «areWeReady» como global, para informar de inmediato que el recurso
  de WebAudio está listo para ser utilizado. */
var radioSource, areWeReady = false;

/* initWebAudio(): Esta función se encarga de inicializar el contexto de
  WebAudio, además de configurar la lógica para la transición desde el sonido
  del modem hacia la reproducción del flujo de audio. */
function initWebAudio(context, modem, radio) {
    /* Creamos una fuente a partir del elemento <audio> para el sonido
      de conexión vía modem. */
    var modemSource = context.createMediaElementSource(modem);
    /* Creamos una fuente a partir del elemento <audio> para el flujo. */
    radioSource = context.createMediaElementSource(radio);

    /* Creamos un nuevo objeto de tipo BufferLoader, con el contexto detectado
      previamente, que contendrá los datos indicados en la lista, y ejecutará
      readyToRun(), una vez cargado todo. */
    var bufferLoader = new BufferLoader(context, ['telephone.wav'],
        readyToRun);

    /* Indicamos que es la primera vez que cargamos todo. Con esto prevenimos
      la doble ejecución de la carga, debido a que en algunos sistema, el
      evento «canplay» es disparado más de una vez. */
    var firstLoad = true;

    /* Una vez el audio del modem esté listo para ser reproducido... */
    modem.addEventListener("canplay", function () {
        /* Mostramos el botón para iniciar la conexión. */
        document.getElementById("patching").style.display = 'none';
        document.getElementById("dialup-connect").style.display = '';
        if (firstLoad) {
            /* Procedemos a cargar asíncronamente el recurso necesario
              para aplicar el efecto. */
            bufferLoader.load();
            /* Nos aseguramos de que todo se ejecute una sola vez. */
            firstLoad = false;
        }
    });
    /* Cada vez que se dispare el evento «timeupdate», se verifica si se ha
      llegado al momento clave del audio, alrededor del segundo 24, para
      iniciar la reproducción del flujo. */
    modem.addEventListener("timeupdate", function () {
        /* La inclusión del booleano «areWeReady» es para estar seguros de
          que se ha cargado todo lo necesario para comenzar a aplicar el
          efecto. */
        if (modem.currentTime >= 24 && areWeReady) {
            radio.play();
        }
        /* Si hemos llegado al segundo 27, volvemos al segundo 24 para hacer
          un loop. Esta parte específica del audio es solo ruido blanco, por
          lo que el bucle no es notorio. */
        if (modem.currentTime >= 27) {
            modem.currentTime = 24;
        }
    });

    /* La siguiente parte es experimental: Intenta continuar con el loop del
      sonido del modem, si por alguna razón la reproducción del flujo de
      audio se detiene. Sólo funciona bien en algunos navegadores. */
    radio.addEventListener("playing", function () {
        modem.pause();
        modem.volume = 0.75;
    });
    radio.addEventListener("waiting", function () {
        modem.play();
    });
    /* Fin de la parte experimental */

    /* Finalmente, nos quedamos a la escucha del evento «click» en el botón de
      conexión, una vez disparado, eliminamos tal escucha para prevenir
      futuras ejecuciones del callback. Lo hacemos a la antigua, asignando
      la función a una variable, para luego jugar con «addEventListener» y
      «removeEventListener». */
    var dialupEl = document.getElementById("dialup-connect");
    var eventFn = function () {
        /* Conectamos el sonido de conexión via modem para ser reproducido
          via WebAudio. */
        modemSource.connect(context.destination);
        /* Reproducimos el sonido de conexión. */
        modem.play(0);
        dialupEl.removeEventListener("click", eventFn);
    };
    dialupEl.addEventListener("click", eventFn);
}

function initHTML5Audio(modem, radio) {
    /* Una vez el audio del modem esté listo para ser reproducido... */
    modem.addEventListener("canplay", function () {
        /* Mostramos el botón para iniciar la conexión. */
        document.getElementById("patching").style.display = 'none';
        document.getElementById("dialup-connect").style.display = '';
    });
    /* Cada vez que se dispare el evento «timeupdate», se verifica si se ha
      llegado al momento clave del audio, alrededor del segundo 24, para
      iniciar la reproducción del flujo. */
    modem.addEventListener("timeupdate", function () {
        if (modem.currentTime >= 24) {
            radio[0].play();
        }
        /* Si hemos llegado al segundo 27, volvemos al segundo 24 para hacer
          un loop. Esta parte específica del audio es solo ruido blanco, por
          lo que el bucle no es notorio. */
        if (modem.currentTime >= 27) {
            modem.currentTime = 24;
        }
    });

    /* La siguiente parte es experimental: Intenta continuar con el loop del
      sonido del modem, si por alguna razón la reproducción del flujo de
      audio se detiene. Sólo funciona bien en algunos navegadores. */
    radio.addEventListener("playing", function () {
        modem.pause();
        modem.volume = 0.75;
    });
    radio.addEventListener("waiting", function () {
        modem.play();
    });
    /* Fin de la parte experimental */

    /* Finalmente, nos quedamos a la escucha del evento «click» en el botón de
      conexión, una vez disparado, eliminamos tal escucha para prevenir
      futuras ejecuciones del callback. Lo hacemos a la antigua, asignando
      la función a una variable, para luego jugar con «addEventListener» y
      «removeEventListener». */
    var dialupEl = document.getElementById("dialup-connect");
    var eventFn = function () {
        /* Reproducimos el sonido de conexión vía modem. */
        modem.play();
        dialupEl.removeEventListener("click", eventFn);
    }
    dialupEl.addEventListener("click", eventFn);
}

/* readyToRun(): Esta función se encarga de conectar el flujo de audio a un
  "convolver", que es un nodo especial para aplicar efectos personalizados
  sobre un buffer de WebAudio. */
function readyToRun(buffers) {
    /* Creamos un convolver.
      (https://developer.mozilla.org/en-US/docs/Web/API/ConvolverNode) */
    var convolver = context.createConvolver();
    /* Le indicamos como buffer el único que tenemos, a saber, el que contiene
      los datos de audio decodificados del archivo "telephone.wav". Este
      archivo "mágico" contiene la onda que se aplica como efecto sobre el
      flujo de audio. Si se deja hasta acá, se obtiene el efecto deseado, a
      saber, "como que el flujo suena a través de un teléfono", pero es muy
      limpio y plano. */
    convolver.buffer = buffers[0];
    /* Conectamos el flujo de audio con el convolver. */
    radioSource.connect(convolver);
    /* Conectamos el convolver con el contexto. «destination» hace referencia
      al nodo de salida del audio. */
    convolver.connect(context.destination);
    /* Para "sazonar" aún más el efecto, usamos una curva de distorsión, que
      se aplica por encima del flujo con el efecto "de teléfono" ya aplicado,
      para hacerlo sonar más "sucio". Primero creamos un generador de ondas
      de audio. */
    var waveShaper = context.createWaveShaper();
    /* Luego le decimos qué tipo de curva queremos generar, a través de una
      función personalizada. */
    waveShaper.curve = makeDistortionCurve();

    /* makeDistortionCurve(): Esta función devuelve una "curva" de distorsión
      para ser usada por el «waveShaper». */
    /* PARA VER LA CURVA Y CÓMO CAMBIA MIRA http://codepen.io/anon/pen/vLdXwy */
    function makeDistortionCurve() {
        /* «k» indica el offset de la distorsión; «n_samples», el espectro
          de muestra, esta debe ser igual al espectro del audio base para
          que no sucedan cosas malas; «curve» almacena la curva de
          distorsión (una lista especial de tipo Float32Array); «deg»
          indica la "altura" de la curva, entendida como qué tan lejos de
          cero se encuentra el punto más alto/bajo de la misma; «i» almacena
          el índice de la iteración y «x» es usado como variable temporal
          para cálculos dentro de la iteración. */
        var k = 30,
            n_samples = 44100,
            curve = new Float32Array(n_samples),
            deg = Math.PI / 180,
            i = 0,
            x;
        for (; i < n_samples; ++i) {
            /* No me pregunten acerca de este entuerto matemático, solo sé
              que genera una curva que se aplica sobre el efecto de
              "teléfono" que mejora bastante más el mismo. */
            x = i * 2 / n_samples - 1;
            curve[i] = (3 + k) * x * 20 * deg / (Math.PI + k * Math.abs(x));
        }
        return curve;
    }

    /* Finalmente, conectamos los nodos. */
    convolver.connect(waveShaper);
    waveShaper.connect(context.destination);

    /* Informamos que estamos listos para ejecutar el flujo de audio. */
    areWeReady = true;
}
