# RadioÑú 1996 #

Esta es la guinda del pastel de
[nuestra broma del día de los inocentes](http://radiognu.org/1996/12/28/presentamos-nueva-version/),
que simula cómo sería escuchar la señal de nuestra radio en 1996. A nuestro
entender:

  * La conexión debería iniciarse vía modem, lo que implica iniciar una
  negociación de entrada a la red, utilizando un cable de teléfono.
  ([56k FTW](https://es.wikipedia.org/wiki/M%C3%B3dem#M.C3.B3dems_telef.C3.B3nicos))
  * Pero antes, se debe "parchar" un cable desde la terminal de llegada del
  servidor, para pasar por un modulador/demodulador propio que se encarga de
  enchufar la señal del flujo de audio a tal cable.
  * Una vez la conexión es formada, sólo se pasa la señal de la radio,
  modularizada por el cable de "parche", a través de la red, para llegar al
  modem, ser demodularizada y reproducida en tu navegador. ¡Tan simple como eso!
  * Desafortunadamente, debido al reducido bus de datos disponible, alguna
  pérdida sonora es esperable. ¡Pero es 1996! ¡Esta es la última tecnología
  disponible!

Si quieres saber lo que realmente sucede trás bambalinas, échale un vistazo a
los comentarios del código. Si algo está incompleto, no está lo suficientemente
bien explicado, o hay algo que debemos corregir, no dudes en
[abrir una incidencia al respecto](https://gitlab.com/radiognu/radiognu-1996/issues/new).

## Sobre el Javascript ##

Aunque hicimos todo lo posible por emular la sensación de estar en 1996, la
tecnología va avanzando, por lo que ciertas cosas que funcionaban en ese tiempo
fueron dejadas de lado para dar paso a cosas mejores. Este es el caso del
Javascript, que ha tenido que alejarse bastante del concepto original propuesto
para que la experiencia fuera funcional y consistente para la gran mayoría de
navegadores disponibles.

La versión publicada originalmente, incluía el uso de jQuery, esto fue para
acelerar el desarrollo en concentrarse en las funcionalidades base. En cambio,
la versión actual, elimina por completo la dependencia, para demostrar (en la
medida de lo posible) cómo se debía programar en la época, para así ser más afín
con el espíritu de este miniproyecto.

Pero hay otro aspecto a tener en cuenta. La funcionalidad principal no sería
posible si no se hiciera un fuerte uso de las últimas tecnologías disponibles
para HTML5, en este caso la
[API WebAudio](https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API),
que permite convertir el navegador en un sintetizador de audio, sin necesidad de
*plugins* adicionales. Así que, irónicamente, se ha tenido que usar lo más
moderno, para dar la impresión de estar en lo más antiguo.

Es a través de ésta tecnología es que se puede aplicar el efecto de estar
escuchando nuestra señal a través de un teléfono, más detalles en el
[archivo de Javascript](https://gitlab.com/radiognu/radiognu-1996/blob/master/1996.js).
